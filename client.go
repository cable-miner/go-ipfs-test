package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"github.com/surma/gobox/pkg/common"
	"os/exec"
	"strings"

	//logging "gx/ipfs/Qmazh5oNUVsDZTs2g59rq8aYQqwpss8tcUWQzor5sCCEuH/go-log"


	
	
	core "github.com/ipfs/go-ipfs/core"
	corenet "github.com/ipfs/go-ipfs/core/corenet"
	fsrepo "github.com/ipfs/go-ipfs/repo/fsrepo"
	peer "gx/ipfs/QmQGwpJy9P4yXZySmqkZEXCmbBpJUb8xntCv8Ca4taZwDC/go-libp2p-peer"

	"gx/ipfs/QmZy2y8t9zQH2a1b8q2ZSLKp17ATuJoCNxxyMFG5qFExpt/go-net/context"
)

func main() {
    if len(os.Args) < 2 {
        fmt.Println("Please give a peer ID as an argument")
        return
    }
    target, err := peer.IDB58Decode(os.Args[1])
    if err != nil {
        panic(err)
    }

    // Basic ipfsnode setup
    r, err := fsrepo.Open("~/.ipfs")
    if err != nil {
        panic(err)
    }

    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

    cfg := &core.BuildCfg{
        Repo:   r,
        Online: true,
    }

    nd, err := core.NewNode(ctx, cfg)

    if err != nil {
        panic(err)
    }

    fmt.Printf("I am peer %s dialing %s\n", nd.Identity, target)
    

	con, err := corenet.Dial(nd, target, "/app/whyrusleeping")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Fprintln(con,"timestamp\n")
	//go io.Copy(os.Stdout, con)
	go io.Copy(os.Stdin, con)
	scanner := bufio.NewScanner(os.Stdin)
        for scanner.Scan() {
		ucl := strings.TrimSpace(scanner.Text())
		//fmt.Println(ucl)
		fmt.Fprintln(con,ucl)
		
			
		}

	
	return
	
	

}

func readIt() {
	reader := bufio.NewReader(os.Stdin)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err != io.EOF {
				//log.Println(fmt.Sprintf("ERROR: %s", err))
			}
			break
		}
		line = strings.TrimSpace(line)
		fmt.Printf("c\t%s\n", line)
		//line
	}	
	return
}

func isComment(line string) bool {
	line = strings.TrimSpace(line)
	return strings.HasPrefix(line, "#")
}

func execute(cmd []string,con *common.BufferedReader) ([]byte,error) {
	if len(cmd) == 0 {
	}
	cmdl,err := exec.Command(cmd[0], cmd[1:]...).Output()
	if err != nil {
		//log.Fatal(err)
	}
	return cmdl,nil
	

}
