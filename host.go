package main

import (
	"fmt"
	"gx/ipfs/QmTYXXfBjAuLdYwikNzwQR6igmsMDeHLHWCrA2htYCbfDP/common/pkg/common"
	"io"
	"os"
	"os/exec"
	"strings"
	//	"io/ioutil"
	"net/http"
	//	"os"
	"github.com/ipfs/go-ipfs/core"
	corenet "github.com/ipfs/go-ipfs/core/corenet"
	"github.com/ipfs/go-ipfs/core/coreunix"
	fsrepo "github.com/ipfs/go-ipfs/repo/fsrepo"
	//	net "gx/ipfs/QmQkQP7WmeT9FRJDsEzAaGYDparttDiB6mCpVBrq2MuWQS/go-libp2p/p2p/net"
	u "gx/ipfs/QmZNVWh8LLjAavuQ2JXuFmuYH3C11xo988vSgp7UQrTRj1/go-ipfs-util"
	"strconv"
	"time"

	"gx/ipfs/QmZy2y8t9zQH2a1b8q2ZSLKp17ATuJoCNxxyMFG5qFExpt/go-net/context"
)

var gnode *core.IpfsNode

func ServeIpfsRand(w http.ResponseWriter, r *http.Request) {
	read := io.LimitReader(u.NewTimeSeededRand(), 2048)

	str, err := coreunix.Add(gnode, read)
	if err != nil {
		w.WriteHeader(504)
		w.Write([]byte(err.Error()))
	} else {
		w.Write([]byte(str))
	}
}

func main() {
	// Basic ipfsnode setup
	r, err := fsrepo.Open("./.ipfshost")
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cfg := &core.BuildCfg{
		Repo:   r,
		Online: true,
	}

	nd, err := core.NewNode(ctx, cfg)

	if err != nil {
		panic(err)
	}

	gnode = nd

	//go printLoop(gnode)
	//	go shell()

	//	message := []byte("hello libp2p!")
	// Set a stream handler on host A

	list, err := corenet.Listen(nd, "/app/whyrusleeping")
	if err != nil {
		panic(err)
	}
	fmt.Printf("I am peer: %s\n", nd.Identity.Pretty())

	for {
		con, err := list.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}
		defer con.Close()
		request := make([]byte, 128) // set maximum request length to 128KB to prevent flood based attacks
		for {
			read_len, err := con.Read(request)

			if err != nil {
				fmt.Println(err)
				break
			}

			if read_len == 0 {
				break // connection already closed by client
			} else if string(request) == "timestamp" {
				daytime := strconv.FormatInt(time.Now().Unix(), 10)
				con.Write([]byte(daytime))
			} else {
				fmt.Printf("data recv \t %s\n", string(request))
				//con.Write([]byte(daytime))
			}

			request = make([]byte, 128) // clear last read content
		}
		fmt.Fprintln(con, "Hello! This is whyrusleepings awesome ipfs service")
		fmt.Printf("Connection from: %s\n", con.Conn().RemotePeer())
	}
	//This is whyrusleepings awesome ipfs service")

	//	http.HandleFunc("/app/whyrusleeping", ServeIpfsRand)
	//	http.ListenAndServe(":8080", nil)

	//list, err := corenet.Listen(nd, "/app/whyrusleeping")
	//if err != nil {
	//	panic(err)
	//}
	// Set the global node for access in the handler

	//
	//	fmt.Printf("reading message")
	//	out, err := ioutil.ReadAll(con)
	//	if err != nil {
	//		log.Fatalln(err)
	//	}
	//	fmt.Printf("client:%s",out)

	//	go io.Copy(os.Stdin, con )

	//		con, err := list.Accept()
	//		if err != nil {
	//			fmt.Println(err)
	//			return
	//		}
	//		defer con.Close()

	//	var in *common.BufferedReader
	//	var out *common.BufferedReader
	//	interactive := true
	//	in = common.NewBufferedReader(os.Stdin)
	//	out = common.NewBufferedReader(os.Stdout)
	//	var e error
	//	var line string

}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}

func isComment(line string) bool {
	line = strings.TrimSpace(line)
	return strings.HasPrefix(line, "#")
}

func execute(cmd []string) ([]byte, error) {
	if len(cmd) == 0 {

	} else {
		cmdl, err := exec.Command(cmd[0], cmd[1:]...).Output()
		if err != nil {
			//log.Fatal(err)
		}
		return cmdl, nil
	}

	return nil, nil
}

func printLoop(nd *core.IpfsNode) {
	list, err := corenet.Listen(nd, "/app/whyrusleeping")
	if err != nil {
		panic(err)
	}

	fmt.Printf("I am peer: %s\n", nd.Identity.Pretty())

	for {
		con, err := list.Accept()
		if err != nil {
			fmt.Println(err)
			return
		}
		defer con.Close()

		fmt.Fprintln(con, "Hello! This is whyrusleepings awesome ipfs service")
		fmt.Printf("Connection from: %s\n", con.Conn().RemotePeer())
	}

}

func shell() {
	var in *common.BufferedReader
	interactive := true
	in = common.NewBufferedReader(os.Stdin)

	var e error
	var line string
	var output []byte
	for e == nil {
		if interactive {
			fmt.Print("> ")
		}
		line, e = in.ReadWholeLine()
		if e != nil {

		}
		if isComment(line) {
			continue
		}
		params, ce := common.Parameterize(line)
		if ce != nil {
			common.DumpError(ce)
			continue
		}

		output, ce = execute(params)
		if ce != nil {
			common.DumpError(ce)
			continue
		}
		fmt.Printf(string(output))
	}
	return

}
